#!/bin/bash
BASE1=/q3774/landscapes-smips
ORG=`pwd`
for d in "v1.0" "v1.1.exp" ; do
  cd $BASE1/$d
  echo "Bringing all dataset files into local cache (workaround gdal-on-DMF bug)"
  files=`find . -iname "*.tif" | sort -n`
  for f in $files ; do
    echo "Enumerating $d/$f"
    head -c 4096 $f > /dev/null
  done
  if [ -f ./tif_list.txt ]; then
    echo "Already creating a tindex? tif_list.txt found."
    exit 1
  fi
  echo "making tif_list for $BASE1/$d/*.tif"
  find . -iname "*.tif" | sort -n > tif_list.txt
  echo "making tindex for $BASE1/$d/tif_list.txt"
  rm -fv "$BASE1/$d/index.shp" "$BASE1/$d/index.dbf" "$BASE1/$d/index.shx"
  gdaltindex ./index.shp --optfile ./tif_list.txt
  rm -fv ./tif_list.txt
done
cd ~/vrt_stats
. .venv/bin/activate
python3 ./add_smips_timestamps.py
cd $PWD
