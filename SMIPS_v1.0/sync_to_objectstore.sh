#!/bin/bash
SWIFT=/home/som05d/.local/bin/swift
BASE=/q3774/landscapes-smips
CONTAINER=landscapes-csiro-smips-public
CREDS=/home/som05d/TERN_DS_applicaiton_cred.sh
. $CREDS

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
    kill -term $$
}

YEAR=$1

#upload v1.0 smips
for f in "index.prj" "index.shp" "index.shx" "index.dbf"; do
    $SWIFT upload --changed --skip-identical --object-name "v1_0/$f" $CONTAINER "$BASE/v1.0/$f"
done
if [ "$YEAR""z" != "z" ]; then
  for l in "totalbucket" "SMindex" ; do
      $SWIFT upload --changed --skip-identical --object-name "v1_0/$l/$YEAR" $CONTAINER "$BASE/v1.0/$l/$YEAR"
  done
else
for y in "2015" "2016" "2017" "2018" "2019" "2020" "2021"; do
    for l in "totalbucket" "SMindex" ; do
        $SWIFT upload --changed --skip-identical --object-name "v1_0/$l/$y" $CONTAINER "$BASE/v1.0/$l/$y"
    done
done
fi

#upload v1.1.exp smips
for f in "index.prj" "index.shp" "index.shx" "index.dbf"; do
    $SWIFT upload --changed --skip-identical --object-name "v1_1_exp/$f" $CONTAINER "$BASE/v1.1.exp/$f"
done
if [ "$YEAR""z" != "z" ]; then
  for l in "totalbucket" "SMindex" ; do
      $SWIFT upload --changed --skip-identical --object-name "v1_1_exp/$l/$YEAR" $CONTAINER "$BASE/v1.1.exp/$l/$YEAR"
  done
else
for y in "2015" "2016" "2017" "2018" "2019" "2020" "2021"; do
    for l in "totalbucket" "SMindex" ; do
        $SWIFT upload --changed --skip-identical --object-name "v1_1_exp/$l/$y" $CONTAINER "$BASE/v1.1.exp/$l/$y"
    done
done
fi