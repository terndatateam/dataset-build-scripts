#!/bin/bash
DOCKER=`which docker`
DOCKER_IMAGE=osgeo/gdal:ubuntu-full-latest
MOUNTS="-v /q3774/landscapes-asc:/q3774/landscapes-asc"
CONTAINER_NAME=asc_run_1
RUNNING=$($DOCKER run --name $CONTAINER_NAME --rm -i -t --detach $MOUNTS $DOCKER_IMAGE tail -f /dev/null)
INFILE="$1"
INDIR="$2"
INFILENAME="$(basename -- $INFILE)"
INPATH="${INFILE%$INFILENAME}"
INPATH="${INPATH#$INDIR}"
OUTDIR="$3"
OUTFILENAME="$OUTDIR""$INPATH""${INFILENAME%.*}.cog.tif"
if [ -f "$OUTFILENAME" ] ; then
  echo "$OUTFILENAME exists"
  exit 0
fi
mkdir -p "$OUTDIR""$INPATH"
NODATA="-a_nodata 0"
COMPRESS="-co COMPRESS=DEFLATE"
ACCESS="Creative Commons Attribution 4.0 (CC By)"
TITLE="Australian Soil Classification Map"
DESCRIPTION="The Australian Soil Map Classification System groups all soils in Australia into 14 Orders. This map is our best estimate of where these soil classes occur. We used a Machine Learning approach to model the spatial distribution of soil classes. This product was developed using observed soil data extracted from the TERN SoilDataFederator - https://esoil.io/TERNLandscapes/SoilDataFederatoR/help/SoilDataFederator.html and a raster environmental covariate stack from TERN - https://drop.ternlandscapes.org.au/#/Covariates/Mosaics."
PROJECT_DESCRIPTION="The TERN Soil and Landscape Grid of Australia Facility has produced a comprehensive fine-resolution grid of soil attributes and important land surface parameters. The data is consistent with the Specifications of the GlobalSoilMap and is managed as part of the Australian Soil Resource Information System (ASRIS)."
CREDIT="Access to this data has been made possible by CSIRO and the Terrestrial Ecosystem Research Network (TERN) which is supported by the Australian Government through the National Collaborative Research Infrastructure Strategy and the Super Science Initiative and by agreement from the data custodians of the background data. All of the organisations listed as collaborating agencies have also contributed significantly to the production of this dataset."
ACK="TERN acknowledges the important contributions of state and federal soil agencies for providing access to soil profile data sets."
ATTRIBUTEACC="The overall map accuracy is 61%. Each pixel also has an estimate of the Random Forest model structural uncertainty represented as a confusion index. The confusion index is available from the same location as this dataset."
COMPLETENESS="The Soil and Landscape Grid of Australia covers all of continental Australia and near coastal islands land areas"
LICENCE_META="LICENCE=http://creativecommons.org/licenses/by/4.0/"
LINEAGE_META="LINEAGE=https://aussoilsdsm.esoil.io/slga-version-2-products/australian-soil-classification-map"
CONTACT_META="CONTACT=esupport@tern.org.au"
CONTACT_E_META="CONTACT_EMAIL=esupport@tern.org.au"
CONTACT_O_META="CONTACT_ORGANISATION=Terrestrial Ecosystem Research Network (TERN)"
CONTACT_P_META="CONTACT_POSTAL=Long Pocket Precinct, Foxtail Building #1019 Level 5 The University of Queensland 80 Meiers Road Indooroopilly QLD 4068 Australia"
CONTACT_S_META="CONTACT_STREET_ADDRESS=Long Pocket Precinct, Foxtail Building #1019 Level 5 The University of Queensland 80 Meiers Road Indooroopilly QLD 4068 Australia"
CUSTODIAN_META="CUSTODIAN=CSIRO"
MAINTENANCE_META="MAINTENANCE_AND_UPDATE_FREQUENCY=Not Planned"
POSITIONALACC="The horizontal positional error is the same as for the raw SRTM 3 second data with 90% of tested locations within 7.2 m for Australia. See Rodriguez et al. (2006) for more information."
ACCESS_META="ACCESS_CONSTRAINTS=$ACCESS"
CREDIT_META="CREDIT=$CREDIT"
ACK_META="ACKNOWLEDGEMENTS=$ACK"
TITLE_META="TITLE=$TITLE"
DTITLE_META="DATASET_TITLE=$TITLE"
DESCRIPTION_META="DESCRIPTION=$DESCRIPTION"
COLLABORATING_META="COLLABORATING_ORGANISATIONS=CSIRO"
ATTRIBUTE_META="ATTRIBUTE_ACCURACY=$ATTRIBUTEACC"
COMPLETE_META="COMPLETENESS=$COMPLETENESS"
D_CREATION_META="DATA_CREATION_DATE=08/08/2021"
D_END_META="DATA_END_DATE=2021"
D_START_META="DATA_START_DATE=1950"
JUR_META="JURISDICTION=Australia"
PROGRESS_META="PROGRESS=Complete"
POINT_CONTACT_META="POINT_OF_CONTACT_PERSON=ross.searle@csiro.au"
POSITIONAL_META="POSITIONAL_ACCURACY=$POSITIONALACC"
PROJ_DESC_META="PROJECT_DESCRIPTION=$PROJECT_DESCRIPTION"
STORED_DATA_META="STORED_DATA_FORMAT=GeoTiff"
RELATED_META="RELATED_LINKS=https://aussoilsdsm.esoil.io/slga-version-2-products/australian-soil-classification-map"
EXEC="$DOCKER exec -i -t $CONTAINER_NAME gdal_translate -stats -sds -of COG $NODATA $COMPRESS -co NUM_THREADS=ALL_CPUS --config GDAL_CACHEMAX 512"
$EXEC -mo "$TITLE_META" -mo "$DTITLE_META" -mo "$DESCRIPTION_META" -mo "$ACCESS_META" -mo "$ATTRIBUTE_META" -mo "$COMPLETE_META" -mo $COLLABORATING_META -mo $LICENCE_META -mo $CUSTODIAN_META \
      -mo $D_CREATION_META -mo $D_END_META -mo $D_START_META -mo $CONTACT_META -mo $CONTACT_E_META -mo "$CONTACT_O_META" -mo "$CONTACT_P_META" -mo "$CONTACT_S_META" \
      -mo "$PROJ_DESC_META" -mo "$RELATED_META" -mo "$CREDIT_META" -mo "$ACK_META" -mo $JUR_META -mo $LINEAGE_META \
      -mo "$MAINTENANCE_META" -mo $POINT_CONTACT_META -mo "$POSITIONAL_META" -mo $PROGRESS_META -mo $STORED_DATA_META \
      $INFILE $OUTFILENAME 
$DOCKER stop $CONTAINER_NAME && $DOCKER kill $CONTAINER_NAME
#echo $EXEC $OTHER_META
