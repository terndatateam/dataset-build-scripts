#!/bin/bash
DOCKER=`which docker`
DOCKER_IMAGE=osgeo/gdal:ubuntu-full-latest
MOUNTS="-v /q3774/landscapes-asc:/q3774/landscapes-asc"
CONTAINER_NAME=asc_run_1
RUNNING=$($DOCKER run --name $CONTAINER_NAME --rm -i -t --detach $MOUNTS $DOCKER_IMAGE tail -f /dev/null)
INFILE="$1"
INDIR="$2"
INFILENAME="$(basename -- $INFILE)"
INPATH="${INFILE%$INFILENAME}"
INPATH="${INPATH#$INDIR}"
OUTDIR="$3"
OUTFILENAME="$OUTDIR""$INPATH""${INFILENAME%.*}.cog.tif"
if [ -f "$OUTFILENAME" ] ; then
  echo "$OUTFILENAME exists"
  exit 0
fi
mkdir -p "$OUTDIR""$INPATH"
NODATA="-a_nodata 0"
COMPRESS="-co COMPRESS=DEFLATE"
TITLE="Modelled Australian Soil Classification Map"
CREDIT="This work was funded by the Terrestrial Ecosystem Research Network (TERN), an Australian Government NCRIS enabled project, and is supported by the use of TERN infrastructure."
ACK="TERN acknowledges the important contributions of state and federal soil agencies for providing access to soil profile data sets."
LICENCE_META="LICENCE=http://creativecommons.org/licenses/by/4.0/"
CONTACT_META="CONTACT=Ross.Searle@csiro.au"
CREDIT_META="CREDIT=$CREDIT"
ACK_META="ACKNOWLEDGMENTS=$ACK"
TITLE_META="TITLE=$TITLE"
EXEC="$DOCKER exec -i -t $CONTAINER_NAME gdal_translate -stats -sds -of COG $NODATA $COMPRESS -co NUM_THREADS=ALL_CPUS --config GDAL_CACHEMAX 512"
$EXEC -mo "$TITLE_META" -mo $LICENCE_META -mo $CONTACT_META -mo "$CREDIT_META" -mo "$ACK_META" $INFILE $OUTFILENAME 
$DOCKER stop $CONTAINER_NAME && $DOCKER kill $CONTAINER_NAME
#echo $EXEC $OTHER_META
