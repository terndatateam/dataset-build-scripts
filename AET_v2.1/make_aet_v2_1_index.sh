#!/bin/bash
BASE1=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_1_newcogs
ORG=`pwd`
cd $BASE1/$d
echo "Bringing all dataset files into local cache (workaround gdal-on-DMF bug)"
files1=`find . -iname "*.tif" | sort -n`
for f in $files1 ; do
  echo "Enumerating $f"
  head -c 4096 $f > /dev/null
done
files2=`find . -iname "*.vrt" | sort -n`
for f in $files2 ; do
  echo "Enumerating $f"
  head -c 4096 $f > /dev/null
done
echo "making tif_list for $BASE1/*.vrt"
find . -iname "*.vrt" | sort -n > tif_list.txt
echo "making tindex for $BASE1/tif_list.txt"
rm -fv "$BASE1/index.shp" "$BASE1/index.dbf" "$BASE1/index.shx"
gdaltindex ./index.shp --optfile ./tif_list.txt
rm -fv ./tif_list.txt
cd ~/vrt_stats
. .venv/bin/activate
echo "Executing ./add_aet_v2_1_timestamps.py"
python3 ./add_aet_v2_1_timestamps.py
cd $PWD
