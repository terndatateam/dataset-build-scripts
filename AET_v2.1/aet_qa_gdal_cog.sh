#!/bin/bash
DOCKER=`which docker`
DOCKER_IMAGE=osgeo/gdal:ubuntu-full-latest
CONTAINER_NAME=aet_run_1
#RUNNING=$($DOCKER run --name $CONTAINER_NAME --rm -i -t --detach $MOUNTS $DOCKER_IMAGE tail -f /dev/null)
#MOUNTS="-v /q3774/landscapes-aet:/q3774/landscapes-aet"
INFILE="$1"
INDIR="$2"
INFILENAME="$(basename -- $INFILE)"
INPATH="${INFILE%$INFILENAME}"
INPATH="${INPATH#$INDIR}"
OUTDIR="$3"
OUTFILENAME="$OUTDIR""$INPATH""${INFILENAME%.*}.cog.tif"
if [ -f "$OUTFILENAME" ] ; then
  echo "$OUTFILENAME exists"
  exit 0
fi
mkdir -p "$OUTDIR""$INPATH"
COMPRESS="-co COMPRESS=DEFLATE"
CREDIT="This work was funded by the Terrestrial Ecosystem Research Network (TERN), an Australian Government NCRIS enabled project, and is supported by the use of TERN infrastructure."
LINEAGE="Data is described in Guerschman, J.P. et al. (2020) Operational spatially-explicit paddock-to-continental actual evapotranspiration estimation: Calibrating the CMRSET algorithm with reflective data from MODIS, VIIRS, Landsat and Sentinel-2 reproduces flux observations and catchment water balance observations across Australia."
PURPOSE="To provide accurate, timely, high-resolution, high-frequency and continuous estimates of actual evapotranspiration (AET) data across Australia to support improved water resource management and environmental management."
LICENCE_META="LICENCE=http://creativecommons.org/licenses/by/4.0/"
CONTACT_META="CONTACT=Tim.Mcvicar@csiro.au"
PURPOSE_META="PURPOSE=$PURPOSE"
#PURPOSE_META="PURPOSE=${PURPOSE// /\\ }"
CREDIT_META="CREDIT=$CREDIT"
#CREDIT_META="CREDIT=${CREDIT// /\\ }"
LINEAGE_META="LINEAGE=$LINEAGE"
#LINEAGE_META="LINEAGE=${LINEAGE// /\\ }"

EXEC="$DOCKER exec -i -t $CONTAINER_NAME gdal_translate -stats -sds -of COG $COMPRESS -co NUM_THREADS=ALL_CPUS --config GDAL_CACHEMAX 512"
exec $EXEC -mo $LICENCE_META -mo $CONTACT_META -mo "$PURPOSE_META" -mo "$CREDIT_META" -mo "$LINEAGE_META" $INFILE $OUTFILENAME 
#echo $EXEC
