#!/bin/bash
SWIFT=/home/som05d/.local/bin/swift
BASE=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_1_newcogs
CONTAINER=landscapes-csiro-aet-public
CONTAINER_PREFIX="v2_1/"
CREDS=/home/som05d/TERN_DS_applicaiton_cred.sh
. $CREDS

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
    kill -term $$
}

YEAR=$1

#upload v2.2 AET
#for f in "index.prj" "index.shp" "index.shx" "index.dbf" "collection.json" "catalog.json" ; do
#    $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$f" $CONTAINER "$BASE/$f"
#done
if [ "$YEAR""z" != "z" ]; then
    $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$YEAR" $CONTAINER "$BASE/$YEAR"
else
    for y in "2012" "2013" "2014"  "2015" "2016" "2017" "2018" "2019" "2020" "2021"; do
        $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$y" $CONTAINER "$BASE/$y"
    done
fi

