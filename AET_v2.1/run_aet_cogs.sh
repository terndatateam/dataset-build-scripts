#!/bin/bash
DOCKER=`which docker`
DOCKER_IMAGE=osgeo/gdal:ubuntu-full-latest
MOUNTS="-v /q3774/landscapes-aet:/q3774/landscapes-aet"
CONTAINER_NAME=aet_run_1
INDIR=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_1/
OUTDIR=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_1_newcogs/
mkdir -p "$OUTDIR"
RUNNING=$(sudo $DOCKER run --name $CONTAINER_NAME --rm -i -t --detach $MOUNTS $DOCKER_IMAGE tail -f /dev/null)
AET_GO=./aet_gdal_cog.sh
AET_QA=./aet_qa_gdal_cog.sh

#sudo find "$INDIR" -type f -iname "*ETa*.tif" -not -iname "*.cog.tif" -exec $AET_GO {} "$INDIR" "$OUTDIR" \;
sudo find "$INDIR" -type f -iname "*pixel_qa*.tif" -not -iname "*.cog.tif" -exec $AET_QA {} "$INDIR" "$OUTDIR" \;

$DOCKER stop $CONTAINER_NAME && $DOCKER rm $CONTAINER_NAME


