#!/bin/bash
DOCKER=`which docker`
DOCKER_IMAGE=osgeo/gdal:ubuntu-full-3.6.3
#MOUNTS="-v /q3774/landscapes-aet:/q3774/landscapes-aet"
CONTAINER_NAME=aet_run_1
#RUNNING=$($DOCKER run --name $CONTAINER_NAME --rm -i -t --detach $MOUNTS $DOCKER_IMAGE tail -f /dev/null)
INFILE="$1"
INDIR="$2"
OVERWRITE="$4"
INFILENAME="$(basename -- $INFILE)"
TMPFILE="/tmp/""$INFILENAME"
INPATH="${INFILE%$INFILENAME}"
INPATH="${INPATH#$INDIR}"
OUTBASE="$3"
OUTPATH="$OUTBASE""$INPATH"
OUTFILENAME="${INFILENAME%.*}.cog.tif"
OUTFILE="$OUTPATH""$OUTFILENAME"
if [ -f "$OUTFILE" ] ; then
  if [ "$OVERWRITE" = "overwrite" ]; then
    rm -f "$OUTFILE" && echo "Deleted existing: $OUTFILE"
  else
    echo "$OUTFILE exists"
    exit 0
  fi
fi
mkdir -p "$OUTPATH"
NODATA="-a_nodata -9999"
SCALE="-scale 0.001"
UNITS="-units mm/d"
COMPRESS="-co COMPRESS=DEFLATE"

CREDIT="This work was funded by the Terrestrial Ecosystem Research Network (TERN), an Australian Government NCRIS enabled project, and is supported by the use of TERN infrastructure."
LINEAGE="Data is described in Guerschman, J.P. et al. (2020) Operational spatially-explicit paddock-to-continental actual evapotranspiration estimation: Calibrating the CMRSET algorithm with reflective data from MODIS, VIIRS, Landsat and Sentinel-2 reproduces flux observations and catchment water balance observations across Australia."
PURPOSE="To provide accurate, timely, high-resolution, high-frequency and continuous estimates of actual evapotranspiration (AET) data across Australia to support improved water resource management and environmental management."
LICENCE_META="LICENCE=http://creativecommons.org/licenses/by/4.0/"
CONTACT_META="CONTACT=Tim.Mcvicar@csiro.au"
PURPOSE_META="PURPOSE=$PURPOSE"
#PURPOSE_META="PURPOSE=${PURPOSE// /\\ }"
CREDIT_META="CREDIT=$CREDIT"
#CREDIT_META="CREDIT=${CREDIT// /\\ }"
LINEAGE_META="LINEAGE=$LINEAGE"
#LINEAGE_META="LINEAGE=${LINEAGE// /\\ }"
# Bring the file into HDD cache on QCIF store
EXEC="$DOCKER exec -i -t $CONTAINER_NAME head -c 4096 $INFILE"
$EXEC > /dev/null
# Copy the file to /tmp inside the container
EXEC="$DOCKER exec -i -t $CONTAINER_NAME cp"
$EXEC $INFILE $TMPFILE
# Run gdal_edit on TMPFILE, to add stats, nodata, units and scale. These are best added here _before_ gdal_translate
EXEC="$DOCKER exec -i -t $CONTAINER_NAME gdal_edit.py -stats $NODATA $SCALE $UNITS --config GDAL_CACHEMAX 512"
$EXEC $TMPFILE
# Don't calculate stats in the translate, and don't add scale to translate, that is already done in gdal_edit
EXEC="$DOCKER exec -i -t $CONTAINER_NAME gdal_translate -sds -of COG $COMPRESS -co NUM_THREADS=ALL_CPUS --config GDAL_CACHEMAX 512"
$EXEC -mo $LICENCE_META -mo $CONTACT_META -mo "$PURPOSE_META" -mo "$CREDIT_META" -mo "$LINEAGE_META" $TMPFILE $OUTFILE
# Delete the file from /tmp inside the container
EXEC="$DOCKER exec -i -t $CONTAINER_NAME rm"
$EXEC $TMPFILE