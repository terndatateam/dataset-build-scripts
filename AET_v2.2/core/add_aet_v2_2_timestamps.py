#!/usr/bin/env python3
import shapefile
import os
import re
from datetime import date
from shutil import copy2

match_date = re.compile(r"(?:\/|\.\/)?(?:.*\/)?\d{4}\/(\d{4}).?(\d{2}).?(\d{2})\/.*\.(tif|vrt)$", flags=re.IGNORECASE)

# Add new SMIPS subdatasets here
#DS = ["CMRSET_LANDSAT_V2_1_newcogs", "CMRSET_LANDSAT_V2_2_newcogs"]
DS = ["CMRSET_LANDSAT_V2_2_newcogs"]

#don't include .shp or .dbf here, suffix is inferred
FILES = [f"/q3774/landscapes-aet/{d}/index" for d in DS]

for FILE in FILES:
    TMPFILE = "{}_tmp".format(FILE)
    # Delete tempfiles...
    try:
        os.unlink("{}.shp".format(TMPFILE))
    except FileNotFoundError:
        pass
    try:
        os.unlink("{}.dbf".format(TMPFILE))
    except FileNotFoundError:
        pass
    try:
        os.unlink("{}.shx".format(TMPFILE))
    except FileNotFoundError:
        pass

    do_swap = False

    with shapefile.Reader(FILE) as shp_in:
        with shapefile.Writer(TMPFILE) as shp_out:
            for f in shp_in.fields:
                if f[0].lower() == "deletionflag":
                    continue
                elif f[0].lower() == "date":
                    continue
                elif f[0].lower() == "layer":
                    continue
                shp_out.field(*f)
            shp_out.field('layer', 'C', 50) # layer name with 50 chars
            shp_out.field('date', 'C', 29) # datetime string (not DATE type) 1234:67:90T23:56:89.123+56:89

            # use stream reading
            for c, sr in enumerate(shp_in.iterShapeRecords()):
                this_parts = sr.record.as_dict()
                location = this_parts.get('location', None)
                if not location:
                    raise RuntimeError("Record in shapefile does not have a 'location'.")
                m = match_date.search(location)
                if not m:
                    raise RuntimeError("Location doesn't have a layer name or datetime component:\n{}".format(location))
                this_date = "{}-{}-{}T00:00:00Z".format(m[1], m[2], m[3])
                if location.startswith("./"):
                    this_parts['location'] = location = location[2:]
                if "_ETa" in location:
                    this_parts['layer'] = "ETa"
                elif "_pixel_qa" in location:
                    this_parts['layer'] = "pixel_qa"
                else:
                    this_parts['layer'] = "unknown"
                this_parts['date'] = this_date
                shp_out.shape(sr.shape)
                shp_out.record(**this_parts)

            # Only replace old db with the new db
            # if we haven't changed the number of records
            if c+1 == shp_out.recNum and c+1 == shp_out.shpNum:
                do_swap = True

    if do_swap:
        os.rename("{}.shp".format(FILE),"{}.shp.old".format(FILE))
        os.rename("{}.dbf".format(FILE),"{}.dbf.old".format(FILE))
        os.rename("{}.shx".format(FILE),"{}.shx.old".format(FILE))
        os.rename("{}.shp".format(TMPFILE),"{}.shp".format(FILE))
        os.rename("{}.dbf".format(TMPFILE),"{}.dbf".format(FILE))
        os.rename("{}.shx".format(TMPFILE),"{}.shx".format(FILE))
        os.unlink("{}.dbf.old".format(FILE))
        os.unlink("{}.shp.old".format(FILE))
        os.unlink("{}.shx.old".format(FILE))