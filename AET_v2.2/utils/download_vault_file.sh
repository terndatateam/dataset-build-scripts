#!/bin/bash
VAULT_ADDR="https://vault.csiro.cloud"
export VAULT_ADDR="$VAULT_ADDR"
VLT_PROJ=acl_vlt_od214808
VLT_ROLE_ID=landscapes_airflow_robot

# Read the secret ID from the configuration file
CONFIG_FILE="/etc/secrets/secrets.conf"
VLT_SECRET_ID=$(awk -F '=' '/secret_id/ {print $2}' "$CONFIG_FILE")
AUTH_RESPONSE=$(vault write auth/approle/$VLT_PROJ/login role_id=$VLT_ROLE_ID secret_id=$VLT_SECRET_ID -format=json)
VAULT_TOKEN=$(echo $AUTH_RESPONSE | jq -r .auth.client_token)
vault login $VAULT_TOKEN
vault kv list -format=json $VLT_PROJ/$VLT_ROLE_ID
touch /tmp/TERN_DS_application_cred_landscapes_csiro_aet_upload.json
vault kv get -address=$VAULT_ADDR -format=json "$VLT_PROJ/$VLT_ROLE_ID/TERN_DS_application_cred_landscapes_csiro_aet_upload" | jq -r '.data.data' > /tmp/TERN_DS_application_cred_landscapes_csiro_aet_upload.json