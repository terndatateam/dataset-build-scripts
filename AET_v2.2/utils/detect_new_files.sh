#!/bin/bash

TOOLS=/q3774/landscapes-aet/22_tools
GS_LOGS=$TOOLS/logs/gs_files_logs

# Get today's date in the format YYYY_MM_DD
date=$(date +%Y_%m_%d)

# Authenticate into GSUTIL using GCP Credentials
cd ~/creds
gcloud auth activate-service-account --key-file upload-robot-79de15a50d2f.json &

pid=$!

wait $pid

# Path to the file storing the previous list of files
PREVIOUS_FILE="previous_files.txt"

# Path to the file storing the current list of files
CURRENT_FILE="current_files.txt"

# Run gsutil ls command to get the current list of files in the bucket
gsutil ls -r gs://aet-datasets/CMRSET_LANDSAT_V2_2 > "${GS_LOGS}/${CURRENT_FILE}"

# Filter out directories from the current file list
grep -v '/$' "${GS_LOGS}/$CURRENT_FILE" > "${GS_LOGS}/${CURRENT_FILE}_filtered"

# Compare the previous and current file lists
diff_output=$(diff "${GS_LOGS}/$PREVIOUS_FILE" "${GS_LOGS}/${CURRENT_FILE}_filtered")
diff_lines=$(echo "$diff_output" | grep "^> " | wc -l)

# Check if the number of new files is greater than 5
if [ "$diff_lines" -gt 5 ]; then
    echo "More than 5 new files detected. Syncing new files for AET dataset..."
    echo "${date}: More than 5 new files detected. Syncing new data..." >> "${GS_LOGS}/detection_logs.txt"
    $TOOLS/aet_dataset_update.sh
else
    echo "No significant amount of new files detected. No sync required for AET dataset."
    echo "${date}: No significant amount of new files detected on AET Google Storage. No sync required." >> "${GS_LOGS}/detection_logs.txt"
fi

# Update the previous file list with the current filtered list for the next run
cp "${GS_LOGS}/${CURRENT_FILE}_filtered" "${GS_LOGS}/$PREVIOUS_FILE"