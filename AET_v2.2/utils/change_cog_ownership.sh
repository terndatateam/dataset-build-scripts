#!/bin/bash

# Set the top-level directory
directory="/q3774/landscapes-aet/CMRSET_LANDSAT_V2_2_newcogs"

# Set the desired new owner
new_owner="q3774"

echo "Performing folder ownership changes for cogs within CMRSET_LANDSAT_V2_2_newcogs from root to q3774..."

# Iterate over the years
for year_dir in "$directory"/*/; do
    if [ -d "$year_dir" ]; then
        echo "Processing year: $(basename "$year_dir")"

        # Change ownership recursively for folders within the year
        sudo find "$year_dir" -type d -user root -exec chown "$new_owner" {} \;
    fi
done

echo "Changes done!"