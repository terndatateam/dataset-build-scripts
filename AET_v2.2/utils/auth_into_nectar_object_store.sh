#!/bin/bash


# Load the JSON File and extract values using jq
auth_data=$(jq -r 'to_entries | .[] | "export \(.key)=\(.value)"' /tmp/TERN_DS_application_cred_landscapes_csiro_aet_upload.json)

# Evaluate the extracted values as environment variables
eval "$auth_data"

eval $(swift auth)