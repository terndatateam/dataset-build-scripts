#!/bin/bash
SWIFT=/usr/local/bin/swift
BASE=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_2_newcogs
TOOLS=/q3774/landscapes-aet/22_tools
CONTAINER=landscapes-csiro-aet-public
CONTAINER_PREFIX="v2_2/"

cd $BASE

# activate virtual environment for python
source ~/vrt_stats/bin/activate

# download vault file for object store credentials
$TOOLS/utils/download_vault_file.sh
# use credentials to authenticate into object store with swift
#source auth_into_nectar_object_store.sh

# Tmp. Drag auth code here in case script isn't properly called.
# Load the JSON File and extract values using jq
auth_data=$(jq -r 'to_entries | .[] | "export \(.key)=\(.value)"' /tmp/TERN_DS_application_cred_landscapes_csiro_aet_upload.json)

# Evaluate the extracted values as environment variables
eval "$auth_data"

# Run swift auth output to set OS_AUTH_TOKEN and OS_STORAGE_URL
eval $(swift auth)

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
    kill -term $$
}

YEAR=$1

if [ "$YEAR""z" == "globz" ]; then
  GLOB="$2"
  if [ "$GLOB""z" == "z" ]; then
      echo "Need glob string please."
      exit 1
  fi
  echo "Finding files matching glob \"$GLOB\""
  files1=`find . -wholename "$GLOB" | sort -n`
  for f in $files1 ; do
    echo "Enumerating $f"
    head -c 4096 $f > /dev/null
  done
  for f in $files1 ; do
      if [ ${f:0:2} == "./" ]; then
          f=${f:2}
      fi
      $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$f" $CONTAINER "$BASE/$f"
  done
  exit 0
fi


#upload v2.2 AET
for f in "index.prj" "index.shp" "index.shx" "index.dbf" "collection.json" "catalog.json" ; do
    $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$f" $CONTAINER "$BASE/$f"
done

if [ "$YEAR""z" != "z" ]; then
    $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$YEAR" $CONTAINER "$BASE/$YEAR"
else
    for y in "2000" "2001" "2002" "2003" "2004" "2005" "2006" "2007" "2008" "2009" "2010" "2011" "2012" "2013" "2014"  "2015" "2016" "2017" "2018" "2019" "2020" "2021" "2022" "2023" "2024"; do
        $SWIFT upload --changed --skip-identical --object-name "$CONTAINER_PREFIX""$y" $CONTAINER "$BASE/$y"
    done
fi

# delete creds file
rm /tmp/TERN_DS_application_cred_landscapes_csiro_aet_upload.json