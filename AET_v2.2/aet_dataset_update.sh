# Author: Aarond Dino
# Purpose: Orchestrate the scripts within 22_Tools in q3774/landscapes-aet to perform the cogification and upload process of AET data from google storage to landscapes-toolbox VM to Nectar Object Store.

TOOLS=/q3774/landscapes-aet/22_tools
LOGS=$TOOLS/logs
DATA=/q3774/landscapes-aet/CMRSET_LANDSAT_V2_2_newcogs/
# Get today's date in the format 12_05_23
date=$(date +%Y_%m_%d)

echo "${date} Log File for AET Dataset Upload Automation File\n\n" > "${LOGS}/${date}_log.txt"

# Authenticate into GSUTIL using GCP Credentials
cd ~/creds
gcloud auth activate-service-account --key-file upload-robot-79de15a50d2f.json &

pid=$!

wait $pid

# Change directory to folder containing all the moving parts (22 Tools) for orchestration
cd $TOOLS

# 1. Sync down the new geotiff files to our temporary storage space
echo "Syncing and downloading new geotiff files available on Google Storage to temporary storage space inside ${DATA}..."
gsutil -m rsync -r gs://aet-datasets/CMRSET_LANDSAT_V2_2 $DATA >> "${LOGS}/${date}_log.txt" 2>&1 &

pid=$!

wait $pid
# 2.a. Wait for process to finish. Then run COG conversion script to convert the geotiffs to COGS.
echo "Running COG conversion of new geotiff files to .cog..."
./core/run_aet_22_cogs.sh >> "${LOGS}/${date}_log.txt" 2>&1 &

wait
# 2.b. Perform ownership checks and changes for newly created cog folders from root to q3774.
echo "Performing ownership check of newly created folders. Change permissions to q3774 from root..."
./utils/change_cog_ownership.sh >> "${LOGS}/${date}_log.txt" 2>&1 &

wait
# 3. Wait for script to finish. Then run the VRT script to create valid GDAL VRT files for the COGs.
echo "Creating valid GDAL VRT files for newly generated cogs..."
python3 ./core/make_vrt_stats.py >> "${LOGS}/${date}_log.txt" 2>&1 &

pid=$!

wait $pid
# 4. Wait for python process to finish. Then create the new/updated gdal shp index file.
echo "Created new/updated GDAL Shape Index file (.shp)..."
./core/make_aet_v2_2_index.sh >> "${LOGS}/${date}_log.txt" 2>&1 &

wait
# 5. Upload new COGs, VRTs, and Index to the Nectar Object Store
echo "Uploading new COGs, VRTs, and Index File to the Necat Object Store..."
./utils/sync_to_objectstore.sh >> "${LOGS}/${date}_log.txt" 2>&1 &

wait

echo "New COGs, VRTs, and Index have all been successfully uploaded to the Nectar Object Store!" >> "${LOGS}/${date}_log.txt"