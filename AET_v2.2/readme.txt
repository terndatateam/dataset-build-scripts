# Authors: Aarond Dino & Ashley Sommer


# aet_dataset_update.sh is the orchestrator that coordinates the syncing
# of new AET dataset geotiffs from google storage, performs conversions to
# COGs, VRT generation, and shape file index creation. The new files are
# then uploaded onto Nectar Object storage.

# This is done by having a detect_new_files.sh script that runs daily to check
# google storage if more than 5 files have changed since the previous check.
# If it finds new files have been added to google storage, the
# aet_dataset_update.sh script is run.

# The script calls the various bash and python scripts located in this
# /q3774/landscapes-aet/22_tools/ directory under core and utils.

# Core consists of the scripts responsible for data processing such as
# cog conversion. Utils consists of scripts that manipulate file ownership,
# download credentials, authenticating into nectar etc.