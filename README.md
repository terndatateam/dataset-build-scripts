This repository contains a collection of scripts and utility applications that are used to produce the datasets published by TERN Landscapes.

These include AET, SMIPS and ASC etc.

These files were copied from the landscapes toolbox vm on the Nectar Cloud.